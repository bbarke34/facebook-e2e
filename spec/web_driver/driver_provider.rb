require 'selenium/webdriver'
require 'singleton'

# Web Driver provider class. This will allow a standardized browser creation for tests
class DriverProvider
  include Singleton

  def initialize
  end

  # Obtain an instance of the driver
  def get_driver
    @driver
  end

  # Starts the web driver
  def start_driver
    @driver = Selenium::WebDriver.for :chrome
    @driver.navigate.to 'https://facebook.com'
  end

  # kills the web driver
  def quit_driver
    @driver.quit
  end

  # Takes a screenshot of the failed page
  def take_screenshot(name)
    puts "Take screenshot with name #{name}"
    @driver.save_screenshot("#{name}.png")
  end
end