require_relative 'page_object'
require 'selenium-webdriver'

class LandingPage < PageObject

  def initialize
    @fields = {
        :first_name => 'firstname',
        :last_name => 'lastname',
        :email => 'reg_email__',
        :email_confirmation => 'reg_email_confirmation__',
        :second_contactpoint => 'reg_second_contactpoint__',
        :passwd => 'reg_passwd__',
        :gender => 'gender_wrapper'
    }
    super()
  end

  def click_sign_up
    @driver.find_element(:css, '.registration_redesign button[type="submit"]').click
    self
  end

  def set_first_name(first_name)
    set_field(:first_name, first_name)
    self
  end

  def set_last_name(last_name)
    set_field(:last_name, last_name)
    self
  end

  def set_email(email)
    set_field(:email, email)
    self
  end

  def set_email_confirmation(email_conf)
    set_field(:email_confirmation, email_conf)

    self
  end

  def set_password(password)
    set_text(:password, password)
    self
  end

  def set_gender(gender)
    css = '[name=\'sex\']'
    case gender
      when "Male"
        @driver.find_element(:css, "#{css}[value='2'").click
      when "Female"
        @driver.find_element(:css, "#{css}[value='1'").click
      else
        raise ArgumentError, "Invalid Gender #{gender}"
    end
  end

  def set_user(user)
    set_first_name(user.first_name)
    set_last_name(user.last_name)
    set_email(user.email)
    set_email_confirmation(user.email)
    set_gender(user.gender)
    self
  end

  # Obtains an array of fields that are currently invalid
  def get_invalid_fields
    invalid_fields = []

    @driver.find_elements(:css, '[aria-describedby]').each {|elm|
      field = @fields.key(elm.attribute('name'))

      if field.nil?
        field = @fields.key(elm.attribute('data-name'))
      end
      puts "#{field} is invalid"
      invalid_fields.push(field)
    }

    return invalid_fields
  end

  private
  # Sets a filed on the page using the @fields has created.
  def set_field(field, text)
    set_text "[name='#{@fields[field]}']", text
  end

end