class PageObject
  def initialize
    @driver = DriverProvider.instance.get_driver
  end

  def set_text(css, text)
    elm = @driver.find_element(:css, css)
    elm.clear
    elm.send_keys(text)
  end
end