require 'rspec'
require_relative '../page_objects/landing_page'
require_relative '../model_objects/user'
require_relative '../web_driver/driver_provider'

describe 'Account Creation' do

  before(:each) do
    DriverProvider.instance.start_driver
  end

  after(:each) do |example|
    driver_provider = DriverProvider.instance

    if example.exception
      driver_provider.take_screenshot(example.metadata[:description])
    end

    driver_provider.quit_driver
  end

  it 'should not create an account if only the first name is filled in' do
    landing_page = LandingPage.new

    invalid_fields = landing_page
        .set_first_name('first')
        .click_sign_up
        .get_invalid_fields

    expect(invalid_fields.length).to eq 4
  end

  it 'should not allow me to sign up if email addresses dont match' do
    landing_page = LandingPage.new
    user = User.new

    invalid_fields = landing_page
        .set_user(user)
        .set_email_confirmation('not.matching@test.com')
        .click_sign_up
        .get_invalid_fields

    expect(invalid_fields).to include(:email_confirmation)
  end

  it 'should fail and take a screenshot' do
    expect(true).to eq false
  end
end