require 'faker'
class User
  attr_accessor :first_name, :last_name, :email, :phone, :password, :birthday, :gender

  def initialize
    self.first_name = Faker::Name.first_name
    self.last_name = Faker::Name.last_name
    self.email = "#{first_name}.#{last_name}@testtest.com"
    self.phone = Faker::PhoneNumber.cell_phone
    self.password = 'Password!'
    self.birthday = Faker::Date::birthday(18, 90)
    self.gender = Faker::Gender.binary_type
  end
end