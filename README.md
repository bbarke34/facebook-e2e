# Facebook Account Creation Test
This project is intended to test the facebook Account Creation feature

## Technologies
* Ruby
* Rspec
* Selenium Webdriver

# Setup
1. run `bundler install`

# Running
Run `rspec spec\tests\account_creation_spec.rb` to execute the test suite.

# Reporting
On failures, a screenshot will be placed in the root of this project. Also, the rspec results will be placed
at the root of this project in `rspec_results.html`
